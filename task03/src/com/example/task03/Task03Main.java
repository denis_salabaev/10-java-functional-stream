package com.example.task03;

import java.util.Comparator;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Stream;

import lombok.NonNull;

public class Task03Main {
  public static void main(String[] args) {
    findMinMax(
      Stream.of(2, 9, 5, 4, 8, 1, 3),
      Integer::compareTo,
      (min, max) -> System.out.println("min: " + min + " / max: " + max)
    );
  }

  public static <T> void findMinMax(
    @NonNull Stream<? extends T> stream,
    @NonNull Comparator<? super T> order,
    @NonNull BiConsumer<? super T, ? super T> minMaxConsumer
  ) {
    var consumer = new Consumer<T>() {
      private T min;
      private T max;
      private int len = 0;

      @Override
      public void accept(T obj) {
        if (len == 0 || order.compare(min, obj) > 0) {
          min = obj;
        }
        if (len == 0 || order.compare(max, obj) < 0) {
          max = obj;
        }
        len++;
      }
    };

    stream.forEach(consumer);
    if (consumer.len != 0) {
      minMaxConsumer.accept(consumer.min, consumer.max);
    } else {
      minMaxConsumer.accept(null, null);
    }
  }
}
