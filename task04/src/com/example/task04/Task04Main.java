package com.example.task04;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Map.Entry;

public class Task04Main {
  public static void main(String[] args) {
    var scanner = new Scanner(System.in, "UTF-8");
    scanner.useDelimiter("[^\\p{L}]+");

    scanner.tokens()
      .map((word) -> word.toLowerCase())
      .reduce(new HashMap<String, Integer>(), (wordsMap, word) -> {
        wordsMap.compute(word, (w, count) -> {
          return (count == null) ? 1 : count + 1;
        });
        return wordsMap;
      }, (w1, w2) -> {
        w1.putAll(w2);
        return w1;
      })
      .entrySet()
      .stream()
      .sorted(
        Comparator.comparing((Entry<String, Integer> entry) -> entry.getValue())
          .reversed()
          .thenComparing((Entry<String, Integer> entry) -> entry.getKey())
      )
      .map((entry) -> entry.getKey())
      .limit(10)
      .forEach((word) -> System.out.println(word));
  }
}
