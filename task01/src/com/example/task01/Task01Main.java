package com.example.task01;

import java.io.IOException;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

import lombok.NonNull;

public class Task01Main {
  public static void main(String[] args) throws IOException {
    // TODO С корректно реализованным методом ternaryOperator должен компилироваться
    // и успешно работать следующий код:
    Predicate<Object> condition = Objects::isNull;
    Function<Object, Integer> ifTrue = obj -> 0;
    Function<CharSequence, Integer> ifFalse = CharSequence::length;
    Function<String, Integer> safeStringLength = ternaryOperator(condition, ifTrue, ifFalse);

    System.out.println(safeStringLength.apply(null));
    System.out.println(safeStringLength.apply(""));
    System.out.println(safeStringLength.apply("Hello World"));
  }

  public static <T, U> Function<T, U> ternaryOperator(
    @NonNull Predicate<? super T> condition,
    @NonNull Function<? super T, ? extends U> ifTrue,
    @NonNull Function<? super T, ? extends U> ifFalse
  ) {
    return (obj) -> {
      if (condition.test(obj)) {
        return ifTrue.apply(obj);
      } else {
        return ifFalse.apply(obj);
      }
    };
  }
}
