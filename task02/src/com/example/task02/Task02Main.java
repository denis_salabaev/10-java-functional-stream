package com.example.task02;

import java.util.function.IntSupplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Task02Main {
  public static void main(String[] args) {
    var list = cycleGrayCode(3).limit(10).boxed().collect(Collectors.toList());
    System.out.println(list);
  }

  public static IntStream cycleGrayCode(int n) {
    if (n < 1 || n > 16) {
      throw new IllegalArgumentException();
    }
    return IntStream.generate(new IntSupplier() {
      private int i = 0;

      @Override
      public int getAsInt() {
        int result = i ^ (i >> 1);
        i = (i + 1) & ((1 << n) - 1);
        return result;
      }
    });
  }
}
